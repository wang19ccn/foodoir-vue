// 菜单 侧边栏
export default [
  // { path: '/index', title: '概览', icon: 'empire' },
  {
    title: '信息管理',
    icon: 'cog',
    children: [
      { path: '/page1', title: '用户信息管理',icon: 'user-circle-o' },
      { path: '/page2', title: '菜类信息管理',icon: 'info-circle' },
      // { path: '/page3', title: '页面 5',icon: 'paper-plane' }
    ]
  },
  { path: '/about', title: '关于', icon: 'star' }
]

import request from '@/plugin/axios'

//将要发送的数据构造成Form Data对象
function paramsSerializer(params) {
  let param = new URLSearchParams()
  for (let i = 0; i < params.length; i++) {
    param.append(params[i].name, params[i].value)
  }
  return param
}

export function getUser (page,size) {
  const data = paramsSerializer([{
    name: 'page',
    value: page
  },{
    name: 'size',
    value: size
  }])
  return request({
    url: 'http://localhost:8848/admin/findPage',
    method: 'post',
    data
  })
}

export function getUserPage (page,size) {
  const data = paramsSerializer([{
    name: 'page',
    value: page
  },{
    name: 'size',
    value: size
  }])
    return request({
      url: 'http://localhost:8848/admin/findPage',
      method: 'post',
      data
    })
  }

export function deleteUser (id) {
  const data = paramsSerializer([{
    name: 'id',
    value: id
  }])
    return request({
      url: 'http://localhost:8848/admin/deleteUser',
      method: 'post',
      data
    })
  }

export function getDishes() {
  const data = paramsSerializer([])
  return request({
    url: 'http://localhost:8848/dishes/findAll',
    method: 'post',
    data
  })
}

export function deleteDishes(dishesId) {
  const data = paramsSerializer([
    {
      name: 'dishesId',
      value: dishesId
    },
  ])
  return request({
    url: 'http://localhost:8848/dishes/deleteDishes',
    method: 'post',
    data
  })
}

export function getToday() {
  const data = paramsSerializer([])
  return request({
    url: 'http://localhost:8848/dishes/getTodayDishes',
    method: 'post',
    data
  })
}

export function setToday(id,todayDishes) {
  const data = paramsSerializer([
    {
      name: 'id',
      value: id
    },
    {
      name: 'todayDishes',
      value: todayDishes
    },
  ])
  return request({
    url: 'http://localhost:8848/dishes/setTodayDishes',
    method: 'post',
    data
  })
}

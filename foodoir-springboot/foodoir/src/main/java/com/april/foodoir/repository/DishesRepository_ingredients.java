package com.april.foodoir.repository;

import com.april.foodoir.entity.Dishes_ingredients;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishesRepository_ingredients extends JpaRepository<Dishes_ingredients,Integer> {
}

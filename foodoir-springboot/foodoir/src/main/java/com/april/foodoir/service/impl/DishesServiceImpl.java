package com.april.foodoir.service.impl;

import com.april.foodoir.entity.*;
import com.april.foodoir.repository.*;
import com.april.foodoir.service.DishesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DishesServiceImpl implements DishesService {
    @Autowired
    DishesRepository dishesRepository;
    @Autowired
    DishesRepository_ingredients dishesRepository_ingredients;
    @Autowired
    DishesRepository_step dishesRepository_step;

    @Autowired
    TodaydishesRepository todaydishesRepository;

    @Autowired
    AttentionRepository attentionRepository;
    @Autowired
    CollectRepository collectRepository;

    /**
     * 菜类全部基本信息
     * @return
     */
    @Override
    public List<Dishes> findAll() {
        return dishesRepository.findAll();
    }

    /**
     * 菜类全部步骤
     * @return
     */
    @Override
    public List<Dishes_step> findStepAll() {
        return dishesRepository_step.findAll();
    }

    @Override
    public List<Dishes_ingredients> findIngredientsAll() {
        return dishesRepository_ingredients.findAll();
    }

    /**
     *
     * @param dishes
     */
    @Override
    public void setDishes(Dishes dishes) {
        dishesRepository.save(dishes);
    }

    /**
     *
     * @param dishesStep
     */
    @Override
    public void setDishesStep(Dishes_step dishesStep) {
        dishesRepository_step.save(dishesStep);
    }

    /**
     *
     * @param dishesIngredients
     */
    @Override
    public void setDishesIngredients(Dishes_ingredients dishesIngredients) {
        dishesRepository_ingredients.save(dishesIngredients);
    }

    @Override
    public void deleteDishes(Integer dishesId) {
        dishesRepository.deleteById(dishesId);
    }

    @Override
    public void deleteDishesStep(Integer stepId) {
        dishesRepository_step.deleteById(stepId);
    }

    @Override
    public void deleteDishesIngredients(Integer ingredientsId) {
        dishesRepository_ingredients.deleteById(ingredientsId);
    }

    @Override
    public void setTodayDishes(Todaydishes todayDishes) {
        todaydishesRepository.save(todayDishes);
    }

    @Override
    public List<Todaydishes> getTodayDishes() {
        return todaydishesRepository.findAll();
    }

    @Override
    public List<Dishes> findAllLike(String dishesName) {
        return dishesRepository.findByDishesNameLike("%"+dishesName+"%");
    }

    /**
     * 服务器时间，用于记录发布时间
     * @return
     */
    @Override
    public String nowtime() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//可以方便地修改日期格式
        String time = dateFormat.format( now );
        //System.out.println(time);
        return time;
    }



}

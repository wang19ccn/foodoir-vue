package com.april.foodoir.repository;

import com.april.foodoir.entity.Collect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CollectRepository extends JpaRepository<Collect,Integer> {
    Collect findByCollectDishesAndCollectUser(Integer dishesId,Integer userId);
    List<Collect> findByCollectUser(Integer userId);

    @Transactional
    void deleteByCollectDishesAndCollectUser(Integer dishesId,Integer userId);
}

package com.april.foodoir.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Collect {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name="collectDishes",insertable = false,updatable = false)
    private Dishes dishes;

    private Integer collectDishes;
    private Integer collectUser;

    public Collect() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCollectDishes() {
        return collectDishes;
    }

    public void setCollectDishes(Integer collectDishes) {
        this.collectDishes = collectDishes;
    }

    public Integer getCollectUser() {
        return collectUser;
    }

    public void setCollectUser(Integer collectUser) {
        this.collectUser = collectUser;
    }
}

package com.april.foodoir.repository;

import com.april.foodoir.entity.Good;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface GoodRepository extends JpaRepository<Good,Integer> {
    Good findByGoodDishesAndAndGoodUser(Integer dishesId,Integer userId);

    @Transactional
    void deleteByGoodDishesAndAndGoodUser(Integer dishesId,Integer userId);
}

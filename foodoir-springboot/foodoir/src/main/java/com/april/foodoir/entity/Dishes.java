package com.april.foodoir.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
public class Dishes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer dishesId;

    private String dishesImg;
    private String dishesName;
    private String dishesDescribe;
    private String dishesTag;
    private Integer dishesGoodnum;
    private Integer dishesCollect;
    private Integer dishesUser;
    private String dishesTime;

    @OneToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "stepDishesid",insertable = false,updatable = false)
    @OrderBy(value = "stepNum ASC")
    private Set<Dishes_step> dishesStep;

    @OneToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "ingredientsDishesid",insertable = false,updatable = false)
    @OrderBy(value = "ingredientsDishesid ASC")
    private Set<Dishes_ingredients> dishesIngredients;

    public Set<Dishes_step> getDishesStep() {
        return dishesStep;
    }

    public void setDishesStep(Set<Dishes_step> dishesStep) {
        this.dishesStep = dishesStep;
    }

    public Set<Dishes_ingredients> getDishesIngredients() {
        return dishesIngredients;
    }

    public void setDishesIngredients(Set<Dishes_ingredients> dishesIngredients) {
        this.dishesIngredients = dishesIngredients;
    }

    public Dishes() {
    }

    public java.lang.Integer getDishesId() {
        return dishesId;
    }

    public void setDishesId(java.lang.Integer dishesId) {
        this.dishesId = dishesId;
    }

    public String getDishesImg() {
        return dishesImg;
    }

    public void setDishesImg(String dishesImg) {
        this.dishesImg = dishesImg;
    }

    public String getDishesName() {
        return dishesName;
    }

    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }

    public String getDishesDescribe() {
        return dishesDescribe;
    }

    public void setDishesDescribe(String dishesDescribe) {
        this.dishesDescribe = dishesDescribe;
    }

    public String getDishesTag() {
        return dishesTag;
    }

    public void setDishesTag(String dishesTag) {
        this.dishesTag = dishesTag;
    }

    public java.lang.Integer getDishesGoodnum() {
        return dishesGoodnum;
    }

    public void setDishesGoodnum(java.lang.Integer dishesGoodnum) {
        this.dishesGoodnum = dishesGoodnum;
    }

    public java.lang.Integer getDishesCollect() {
        return dishesCollect;
    }

    public void setDishesCollect(java.lang.Integer dishesCollect) {
        this.dishesCollect = dishesCollect;
    }

    public Integer getDishesUser() {
        return dishesUser;
    }

    public void setDishesUser(Integer dishesUser) {
        this.dishesUser = dishesUser;
    }

    public String getDishesTime() {
        return dishesTime;
    }

    public void setDishesTime(String dishesTime) {
        this.dishesTime = dishesTime;
    }

//    public List<Dishes_step> getDishes_stepList() {
//        return dishes_stepList;
//    }
//
//    public void setDishes_stepList(List<Dishes_step> dishes_stepList) {
//        this.dishes_stepList = dishes_stepList;
//    }
}

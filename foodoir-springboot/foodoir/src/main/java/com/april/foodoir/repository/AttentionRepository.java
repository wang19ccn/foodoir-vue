package com.april.foodoir.repository;

import com.april.foodoir.entity.Attention;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AttentionRepository extends JpaRepository<Attention,Integer>{
    Attention findByAccountIdaAndAccountIdb(Integer idA,Integer idB);
    List<Attention> findByAccountIda(Integer ida);
    List<Attention> findByAccountIdb(Integer idb);

    @Transactional
    void deleteByAccountIdaAndAccountIdb(Integer idA,Integer idB);
}

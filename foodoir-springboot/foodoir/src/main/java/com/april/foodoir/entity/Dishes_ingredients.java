package com.april.foodoir.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Dishes_ingredients {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ingredientsId;

    private Integer ingredientsDishesid;
    private String ingredientsName;
    private String ingredientsNum;
    private String ingredientsUnit;

    public Dishes_ingredients() {
    }

    public Integer getIngredients_id() {
        return ingredientsId;
    }

    public void setIngredients_id(Integer ingredients_id) {
        this.ingredientsId = ingredients_id;
    }

    public Integer getIngredients_dishesid() {
        return ingredientsDishesid;
    }

    public void setIngredients_dishesid(Integer ingredients_dishesid) {
        this.ingredientsDishesid = ingredients_dishesid;
    }

    public String getIngredientsName() {
        return ingredientsName;
    }

    public void setIngredientsName(String ingredientsName) {
        this.ingredientsName = ingredientsName;
    }

    public String getIngredientsNum() {
        return ingredientsNum;
    }

    public void setIngredientsNum(String ingredientsNum) {
        this.ingredientsNum = ingredientsNum;
    }

    public String getIngredientsUnit() {
        return ingredientsUnit;
    }

    public void setIngredientsUnit(String ingredientsUnit) {
        this.ingredientsUnit = ingredientsUnit;
    }
}

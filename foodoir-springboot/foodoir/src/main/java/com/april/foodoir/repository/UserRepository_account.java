package com.april.foodoir.repository;

import com.april.foodoir.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository_account extends JpaRepository<Account,Integer> {
    Account findByUsername(String username);
    Account getByUsernameAndAndPassword(String username,String password);
}

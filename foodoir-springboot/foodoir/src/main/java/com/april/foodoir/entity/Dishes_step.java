package com.april.foodoir.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Dishes_step {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stepId;

    private Integer stepDishesid;

//    @ManyToOne
//    @JoinColumn(name="dishes_id")
//    private Dishes dishes;

    private String stepImg;
    private String stepContent;
    private Integer stepNum;

//    @ManyToOne(cascade = CascadeType.REFRESH, optional = false)
//    @JoinColumn(name = "stepDishesid",referencedColumnName="dishesId",insertable = false,updatable = false)
//    private Dishes dishes;
//
//    public Dishes getDishes() {
//        return dishes;
//    }
//
//    public void setDishes(Dishes dishes) {
//        this.dishes = dishes;
//    }

    public Dishes_step() {
    }

    public Integer getStepId() {
        return stepId;
    }

    public void setStepId(Integer stepId) {
        this.stepId = stepId;
    }

    public Integer getStepDishesid() {
        return stepDishesid;
    }

    public void setStepDishesid(Integer stepDishesid) {
        this.stepDishesid = stepDishesid;
    }

    public String getStepImg() {
        return stepImg;
    }

    public void setStepImg(String stepImg) {
        this.stepImg = stepImg;
    }

    public String getStepContent() {
        return stepContent;
    }

    public void setStepContent(String stepContent) {
        this.stepContent = stepContent;
    }

    public Integer getStepNum() {
        return stepNum;
    }

    public void setStepNum(Integer stepNum) {
        this.stepNum = stepNum;
    }
}

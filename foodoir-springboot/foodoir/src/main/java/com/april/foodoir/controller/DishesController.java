package com.april.foodoir.controller;

import com.april.foodoir.entity.Dishes;
import com.april.foodoir.entity.Dishes_ingredients;
import com.april.foodoir.entity.Dishes_step;
import com.april.foodoir.entity.Todaydishes;
import com.april.foodoir.service.DishesService;
import com.april.foodoir.util.FileNameUtil;
import com.april.foodoir.util.FileUploadUtil;
import com.april.foodoir.util.StringUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/dishes")
public class DishesController {
    @Autowired
    DishesService dishesService;

    /**
     * 全部菜类
     * @return
     */
    @RequestMapping(value = "/findAll",method = RequestMethod.POST)
    public List<Dishes> findAll(){
        return dishesService.findAll();
    }

    /**
     * 全部步骤
     * @return
     */
    @RequestMapping(value = "/findStepAll",method = RequestMethod.POST)
    public List<Dishes_step> findStepAll(){
        return dishesService.findStepAll();
    }

    /**
     * 全部配料
     * @return
     */
    @RequestMapping(value = "/findIngredientsAll",method = RequestMethod.POST)
    public List<Dishes_ingredients> findIngredientsAll(){
        return dishesService.findIngredientsAll();
    }


    /**
     * 获取菜类
     * @param dishes
     * @return
     */
    @RequestMapping(value = "/setDishes",method = RequestMethod.POST)
    public Integer setDishes(Dishes dishes){
        dishesService.setDishes(dishes);
        return dishes.getDishesId();
    }

    /**
     * 获取步骤
     * @param dishes_step
     */
    @RequestMapping(value = "/setDishesStep",method = RequestMethod.POST)
    public void setDishesStep(Dishes_step dishes_step){
        dishesService.setDishesStep(dishes_step);
    }

    /**
     * 获取配料
     * @param dishes_ingredients
     */
    @RequestMapping(value = "/setDishesIngredients",method = RequestMethod.POST)
    public void setDishesIngredients(Dishes_ingredients dishes_ingredients){
        dishesService.setDishesIngredients(dishes_ingredients);
    }

    /**
     * 删除菜类
     * @param dishesId
     */
    @RequestMapping(value = "/deleteDishes",method = RequestMethod.POST)
    public List<Dishes> deleteDishes(Integer dishesId){
        dishesService.deleteDishes(dishesId);
        return dishesService.findAll();
    }

    /**
     * 删除步骤
     * @param dishesStepId
     */
    @RequestMapping(value = "/deleteStep",method = RequestMethod.POST)
    public void deleteStep(Integer dishesStepId){
        dishesService.deleteDishesStep(dishesStepId);
    }

    /**
     * 删除配料
     * @param dishesIngredientsId
     */
    @RequestMapping(value = "/deleteIngredients",method = RequestMethod.POST)
    public void deleteIngredients(Integer dishesIngredientsId){
        dishesService.deleteDishesIngredients(dishesIngredientsId);
    }

    /**
     * 模糊查询菜类
     * @param dishesName
     * @return
     */
    @RequestMapping(value = "/likeDishes",method = RequestMethod.POST)
    public List<Dishes> likeDishes(String dishesName){
        return  dishesService.findAllLike(dishesName);
    }

    @RequestMapping(value = "/setTodayDishes",method = RequestMethod.POST)
    public void setTodayDishes(Todaydishes todayDishes){
        dishesService.setTodayDishes(todayDishes);
    }

    @RequestMapping(value = "/getTodayDishes",method = RequestMethod.POST)
    public List<Todaydishes> getTodayDishes(){
        return dishesService.getTodayDishes();
    }


    /**
     * 获取服务器时间
     * @return
     */
    @RequestMapping(value = "/time",method = RequestMethod.POST)
    public String time(){
        return dishesService.nowtime();
    }

    /**
     * 图片上传
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/upimg",method = RequestMethod.POST)
    public String coversUpload(MultipartFile file) throws Exception {
        String folder = "D:/test";
        File imageFolder = new File(folder);
        String fileName = file.getOriginalFilename();
        fileName = FileNameUtil.getFileName(fileName);
        //File f = new File(imageFolder, StringUtils.getRandomString(6) + file.getOriginalFilename()
              //  .substring(file.getOriginalFilename().length() - 4)); //存在bug jpeg格式 无法正常保存后缀
        File f = new File(imageFolder,fileName);
        if (!f.getParentFile().exists())
            f.getParentFile().mkdirs();
        try {
            file.transferTo(f);
            String imgURL = "http://localhost:8848/dishes/upimg/" + f.getName();
            return imgURL;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}

package com.april.foodoir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodoirApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodoirApplication.class, args);
    }

}

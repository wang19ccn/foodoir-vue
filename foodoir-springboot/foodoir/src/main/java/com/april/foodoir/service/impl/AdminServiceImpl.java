package com.april.foodoir.service.impl;

import com.april.foodoir.entity.Account_info;
import com.april.foodoir.repository.UserRepository;
import com.april.foodoir.repository.UserRepository_account;
import com.april.foodoir.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRepository_account userRepository_account;

    //查找用户全部信息
    @Override
    public List<Account_info> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<Account_info> findPage(Integer page, Integer size) {
        PageRequest request= PageRequest.of(page,size);
        return userRepository.findAll(request);
    }

    @Override
    public String deleteUser(Integer id) {
        userRepository_account.deleteById(id);
        return "删除成功！";
    }
}

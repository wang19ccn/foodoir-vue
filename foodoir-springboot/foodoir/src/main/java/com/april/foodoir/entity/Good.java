package com.april.foodoir.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Good {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer goodDishes;
    private Integer goodUser;

    public Good() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodDishes() {
        return goodDishes;
    }

    public void setGoodDishes(Integer goodDishes) {
        this.goodDishes = goodDishes;
    }

    public Integer getGoodUser() {
        return goodUser;
    }

    public void setGoodUser(Integer goodUser) {
        this.goodUser = goodUser;
    }
}

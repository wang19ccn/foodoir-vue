package com.april.foodoir.repository;

import com.april.foodoir.entity.Account_info;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Account_info,Integer> {
    Account_info findByAccountId(Integer id);
}


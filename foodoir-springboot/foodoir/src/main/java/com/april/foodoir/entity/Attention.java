package com.april.foodoir.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Attention {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name="accountIda",insertable = false,updatable = false)
    private Account_info account_infoa;

    @OneToOne
    @JoinColumn(name="accountIdb",insertable = false,updatable = false)
    private Account_info account_infob;

    private Integer accountIda;//信息表的主键
    private Integer accountIdb;//信息表的主键

    public Attention() {
    }

    public Account_info getAccount_infoa() {
        return account_infoa;
    }

    public void setAccount_infoa(Account_info account_infoa) {
        this.account_infoa = account_infoa;
    }

    public Account_info getAccount_infob() {
        return account_infob;
    }

    public void setAccount_infob(Account_info account_infob) {
        this.account_infob = account_infob;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountIda() {
        return accountIda;
    }

    public void setAccountIda(Integer accountIda) {
        this.accountIda = accountIda;
    }

    public Integer getAccountIdb() {
        return accountIdb;
    }

    public void setAccountIdb(Integer accountIdb) {
        this.accountIdb = accountIdb;
    }
}

package com.april.foodoir.controller;

import com.april.foodoir.entity.Account_info;
import com.april.foodoir.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/admin")
public class AdminUserController {
    @Autowired
    private AdminService admminService;

    @ApiOperation(value = "findAll", notes = "findAll")
    @RequestMapping(value = "/findAll",method = RequestMethod.GET)  //前端使用时改回post请求
    public List<Account_info> findAll(){
        return admminService.findAll();
    }

    @ApiOperation(value = "findPage", notes = "findPage")
    @RequestMapping(value ="/findPage",method = RequestMethod.POST)
    public Page<Account_info> findPage(@RequestParam("page") String page, @RequestParam("size") String size){
        return admminService.findPage(Integer.parseInt(page),Integer.parseInt(size));
    }

    @RequestMapping(value ="/deleteUser",method = RequestMethod.POST)
    public String deleteById(@RequestParam("id") String id){
        return admminService.deleteUser(Integer.parseInt(id));
    }
}

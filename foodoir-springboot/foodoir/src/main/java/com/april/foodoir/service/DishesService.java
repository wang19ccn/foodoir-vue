package com.april.foodoir.service;

import com.april.foodoir.entity.*;

import java.util.List;

public interface DishesService {
    //获取菜类相关信息
    public List<Dishes> findAll();
    public List<Dishes_step> findStepAll();
    public List<Dishes_ingredients> findIngredientsAll();

    //存--菜类信息
    public void setDishes(Dishes dishes);
    //存--菜类步骤
    public void setDishesStep(Dishes_step dishesStep);
    //存--菜类配料
    public void setDishesIngredients(Dishes_ingredients dishesIngredients);

    //删除菜类(全部删除）
    public void deleteDishes(Integer dishesId);
    //删除步骤
    public void deleteDishesStep(Integer stepId);
    //删除配料
    public void deleteDishesIngredients(Integer ingredientsId);

    //设置今日推荐
    public void setTodayDishes(Todaydishes todayDishes);
    //获取今日推荐
    public List<Todaydishes> getTodayDishes();

    //模糊查询菜类信息
    public List<Dishes> findAllLike(String dishesName);

    //获取服务器时间
    public String nowtime();
}

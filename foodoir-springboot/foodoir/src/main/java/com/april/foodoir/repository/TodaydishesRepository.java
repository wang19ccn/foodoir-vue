package com.april.foodoir.repository;

import com.april.foodoir.entity.Todaydishes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodaydishesRepository extends JpaRepository<Todaydishes,Integer> {
}

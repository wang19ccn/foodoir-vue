package com.april.foodoir.repository;

import com.april.foodoir.entity.Dishes_step;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishesRepository_step extends JpaRepository<Dishes_step,Integer> {
}

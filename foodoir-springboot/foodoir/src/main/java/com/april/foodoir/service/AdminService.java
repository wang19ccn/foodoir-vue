package com.april.foodoir.service;

import com.april.foodoir.entity.Account_info;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AdminService {

    //----------用户信息管理-----------------

    //查询用户信息表所有信息
    public List<Account_info> findAll();

    //分页查询用户信息表所有信息
    public Page<Account_info> findPage(Integer page, Integer size);

    //删除用户信息
    public String deleteUser(Integer id);

}

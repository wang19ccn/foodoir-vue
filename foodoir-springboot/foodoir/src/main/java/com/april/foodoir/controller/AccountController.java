package com.april.foodoir.controller;

import com.april.foodoir.entity.*;
import com.april.foodoir.service.AccountService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

/**
 * 账号管理
 */
@Controller
public class AccountController {
    @Autowired
    AccountService accountService;

    /**
     * 登录
     */
    @CrossOrigin
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public Integer login(Account requestUser) {
        // 对 html 标签进行转义，防止 XSS 攻击
        String username = requestUser.getUsername();
        username = HtmlUtils.htmlEscape(username);

        Account account=accountService.loginAccount(username,requestUser.getPassword());
        if(null == account){
            return 400;
        }else{
            return account.getUserId();
        }
    }

    /**
     * 注册--检查账号名是否已经存在
     * @param username
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/checkUsername",method = RequestMethod.POST)
    @ResponseBody
    public boolean checkUsername(String username) {
        Account account=accountService.checkUsername(username);
        if(null == account){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 注册--创建账号和密码，并返回用户ID号
     * @param requestUser
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/logonAccount",method = RequestMethod.POST)
    @ResponseBody
    public Integer logonAccount(Account requestUser) {
        accountService.logonAccount(requestUser);
        return requestUser.getUserId();
    }

    /**
     * 注册--创建用户相关信息
     * @param UserInfo
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/logonInfo",method = RequestMethod.POST)
    @ResponseBody
    public String logonInfo(Account_info UserInfo) {
        accountService.logonInfo(UserInfo);
        return null;
    }

    /**
     * 获取用户的信息
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/getInfo",method = RequestMethod.POST)
    @ResponseBody
    public Account_info getInfo(@RequestParam("userId") Integer userId) {
        return accountService.getInfo(userId);
    }

    /**
     * 保存--关注
     * @param attention
     */
    @CrossOrigin
    @RequestMapping(value ="/setAttention",method = RequestMethod.POST)
    @ResponseBody
    public void setAttention(Attention attention) {
        accountService.setAttention(attention);
    }

    /**
     * 保存--收藏
     * @param collect
     */
    @CrossOrigin
    @RequestMapping(value ="/setCollect",method = RequestMethod.POST)
    @ResponseBody
    public void setCollect(Collect collect) {
        accountService.setCollect(collect);
    }

    /**
     * 保存--点赞
     * @param good
     */
    @CrossOrigin
    @RequestMapping(value ="/setGood",method = RequestMethod.POST)
    @ResponseBody
    public void setGood(Good good) {
        accountService.setGood(good);
    }

    /**
     * 获取当前登录用户的关注列表
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/getAttention",method = RequestMethod.POST)
    @ResponseBody
    public List<Attention> getAttention(@RequestParam("userId") Integer userId){
        return accountService.getAttention(userId);
    }

    /**
     * 获取当前登录用户的粉丝列表
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/getFans",method = RequestMethod.POST)
    @ResponseBody
    public List<Attention> getgetFans(@RequestParam("userId") Integer userId){
        return accountService.getFans(userId);
    }

    /**
     * 获取当前登录用户的收藏列表
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/getCollect",method = RequestMethod.POST)
    @ResponseBody
    public List<Collect> getCollect(@RequestParam("userId") Integer userId){
        return accountService.getCollect(userId);
    }


    /**
     * 检查是否已经关注
     * @param ida
     * @param idb
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/checkAttention",method = RequestMethod.POST)
    @ResponseBody
    public boolean checkAttention(Integer ida, Integer idb) {
        Attention attention = accountService.checkAttention(ida,idb);
        if(null == attention){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 检查是否已经收藏
     * @param dishesId
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/checkCollect",method = RequestMethod.POST)
    @ResponseBody
    public boolean checkCollect(Integer dishesId, Integer userId) {
        Collect collect = accountService.checkCollect(dishesId,userId);
        if(null == collect){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 检查是否点赞
     * @param dishesId
     * @param userId
     * @return
     */
    @CrossOrigin
    @RequestMapping(value ="/checkGood",method = RequestMethod.POST)
    @ResponseBody
    public boolean checkGood(Integer dishesId, Integer userId) {
        Good good=accountService.checkGood(dishesId,userId);
        if(null == good){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 删除关注
     * @param ida
     * @param idb
     */
    @CrossOrigin
    @RequestMapping(value ="/deleteAttention",method = RequestMethod.POST)
    @ResponseBody
    public List<Attention> deleteAttention(Integer ida, Integer idb) {
        accountService.deleteAttention(ida, idb);
        return accountService.getAttention(ida);
    }

    /**
     * 删除收藏
     * @param dishesId
     * @param userId
     */
    @CrossOrigin
    @RequestMapping(value ="/deleteCollect",method = RequestMethod.POST)
    @ResponseBody
    public List<Collect> deleteCollect(Integer dishesId, Integer userId) {
        accountService.deleteCollect(dishesId, userId);
        return accountService.getCollect(userId);
    }

    /**
     * 删除点赞
     * @param dishesId
     * @param userId
     */
    @CrossOrigin
    @RequestMapping(value ="/deleteGood",method = RequestMethod.POST)
    @ResponseBody
    public void deleteGood(Integer dishesId, Integer userId) {
        accountService.deleteGood(dishesId, userId);
    }
}

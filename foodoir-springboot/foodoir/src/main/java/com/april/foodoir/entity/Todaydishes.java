package com.april.foodoir.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Todaydishes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name="todayDishes",insertable = false,updatable = false)
    private Dishes dishes;

    private Integer todayDishes;

    public Todaydishes() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Dishes getDishes() {
        return dishes;
    }

    public void setDishes(Dishes dishes) {
        this.dishes = dishes;
    }

    public Integer getTodayDishes() {
        return todayDishes;
    }

    public void setTodayDishes(Integer todayDishes) {
        this.todayDishes = todayDishes;
    }
}

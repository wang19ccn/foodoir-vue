package com.april.foodoir.entity;

import javax.persistence.*;

@Entity
public class Account_info {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer infoId;

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name="userId")
//    private Account account;

    private Integer accountId;
    private String infoImg;
    private String infoNickname;
    private String infoBirthday;
    private String infoSex;
    private String infoIntro;

    public Account_info() {
    }

    public Integer getInfoId() {
        return infoId;
    }

    public void setInfoId(Integer infoId) {
        this.infoId = infoId;
    }

//    public Account getAccount() {
//        return account;
//    }
//
//    public void setAccount(Account account) {
//        this.account = account;
//    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }



    public String getInfoImg() {
        return infoImg;
    }

    public void setInfoImg(String infoImg) {
        this.infoImg = infoImg;
    }

    public String getInfoNickname() {
        return infoNickname;
    }

    public void setInfoNickname(String infoNickname) {
        this.infoNickname = infoNickname;
    }

    public String getInfoBirthday() {
        return infoBirthday;
    }

    public void setInfoBirthday(String infoBirthday) {
        this.infoBirthday = infoBirthday;
    }

    public String getInfoSex() {
        return infoSex;
    }

    public void setInfoSex(String infoSex) {
        this.infoSex = infoSex;
    }

    public String getInfoIntro() {
        return infoIntro;
    }

    public void setInfoIntro(String infoIntro) {
        this.infoIntro = infoIntro;
    }
}

package com.april.foodoir.service;

import com.april.foodoir.entity.*;
import io.swagger.models.auth.In;

import java.util.List;

public interface AccountService {
    //登录
    public Account loginAccount(String username,String password);

    //注册
    public Account checkUsername(String username);
    public String logonAccount(Account account);
    public String logonInfo(Account_info account_info);

    //获取用户的数据
    public Account_info getInfo(Integer id);

    //保存--关注
    public void setAttention(Attention attention);

    //保存--收藏
    public void setCollect(Collect collect);

    //保存--点赞
    public void setGood(Good good);

    //获取当前用户关注列表
    public List<Attention> getAttention(Integer id);

    //获取当前用户粉丝列表
    public List<Attention> getFans(Integer id);

    //获取当前用户收藏列表
    public List<Collect> getCollect(Integer id);

    //检测是否已经关注
    public Attention checkAttention(Integer ida,Integer idb);

    //检测是否已经收藏
    public Collect checkCollect(Integer dishesId,Integer userId);

    //检测是否已经点赞
    public Good checkGood(Integer dishesId,Integer userId);

    //删除关注
    public void deleteAttention(Integer ida,Integer idb);

    //删除收藏
    public void deleteCollect(Integer dishesId,Integer userId);

    //删除点赞
    public void deleteGood(Integer dishesId,Integer userId);
}

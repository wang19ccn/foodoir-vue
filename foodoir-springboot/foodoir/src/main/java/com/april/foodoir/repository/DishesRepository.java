package com.april.foodoir.repository;

import com.april.foodoir.entity.Dishes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DishesRepository  extends JpaRepository<Dishes,Integer>{
    List<Dishes> findByDishesNameLike(String dishesName);
}

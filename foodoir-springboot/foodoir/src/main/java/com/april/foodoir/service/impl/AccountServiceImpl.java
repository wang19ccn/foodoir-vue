package com.april.foodoir.service.impl;

import com.april.foodoir.entity.*;
import com.april.foodoir.repository.*;
import com.april.foodoir.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    UserRepository_account userRepository_account;
    @Autowired
    UserRepository userRepository;

    @Autowired
    AttentionRepository attentionRepository;
    @Autowired
    CollectRepository collectRepository;
    @Autowired
    GoodRepository goodRepository;

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    @Override
    public Account loginAccount(String username, String password) {
        return userRepository_account.getByUsernameAndAndPassword(username,password);
    }

    /**
     * 注册--检查账号名是否存在
     * @param username
     * @return
     */
    @Override
    public Account checkUsername(String username) {
        return userRepository_account.findByUsername(username);
    }

    /**
     * 注册--存账号密码
     * @param account
     * @return
     */
    @Override
    public String logonAccount(Account account) {
        userRepository_account.save(account);
        return null;
    }

    /**
     * 注册--存账号相关信息
     * @param account_info
     * @return
     */
    @Override
    public String logonInfo(Account_info account_info) {
        userRepository.save(account_info);
        return null;
    }

    /**
     * 获取用户信息
     * @param id
     * @return
     */
    @Override
    public Account_info getInfo(Integer id) {
        return userRepository.findByAccountId(id);
    }

    /**
     * 保存关注信息
     * @param attention
     */
    @Override
    public void setAttention(Attention attention) {
        attentionRepository.save(attention);
    }

    /**
     * 保存收藏信息
     * @param collect
     */
    @Override
    public void setCollect(Collect collect) {
        collectRepository.save(collect);
    }

    @Override
    public void setGood(Good good) {
        goodRepository.save(good);
    }

    /**
     * 获取当前登录用户的关注列表
     * @param id
     * @return
     */
    @Override
    public List<Attention> getAttention(Integer id) {
        return attentionRepository.findByAccountIda(id);
    }

    /**
     * 获取当前登录用户的粉丝列表
     * @param id
     * @return
     */
    @Override
    public List<Attention> getFans(Integer id) {
        return attentionRepository.findByAccountIdb(id);
    }

    /**
     * 获取当前登录用户的收藏列表
     * @param id
     * @return
     */
    @Override
    public List<Collect> getCollect(Integer id) {
        return collectRepository.findByCollectUser(id);
    }

    /**
     * 检测是否关注
     * @param ida
     * @param idb
     * @return
     */
    @Override
    public Attention checkAttention(Integer ida, Integer idb) {
        return attentionRepository.findByAccountIdaAndAccountIdb(ida,idb);
    }

    /**
     * 检测是否收藏
     * @param dishesId
     * @param userId
     * @return
     */
    @Override
    public Collect checkCollect(Integer dishesId, Integer userId) {
        return collectRepository.findByCollectDishesAndCollectUser(dishesId,userId);
    }

    @Override
    public Good checkGood(Integer dishesId, Integer userId) {
        return goodRepository.findByGoodDishesAndAndGoodUser(dishesId,userId);
    }

    @Override
    public void deleteAttention(Integer ida, Integer idb) {
        attentionRepository.deleteByAccountIdaAndAccountIdb(ida,idb);
    }

    @Override
    public void deleteCollect(Integer dishesId, Integer userId) {
        collectRepository.deleteByCollectDishesAndCollectUser(dishesId,userId);
    }

    @Override
    public void deleteGood(Integer dishesId, Integer userId) {
        goodRepository.deleteByGoodDishesAndAndGoodUser(dishesId,userId);
    }
}

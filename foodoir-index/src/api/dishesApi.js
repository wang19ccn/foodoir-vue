import axios from 'axios'

//将要发送的数据构造成Form Data对象
function paramsSerializer(params) {
    let param = new URLSearchParams()
    for (let i = 0; i < params.length; i++) {
        param.append(params[i].name, params[i].value)
    }
    return param
}


//获取菜类信息
export function getDishes() {
    const data = paramsSerializer([])
    return axios.request({
        url: 'http://localhost:8848/dishes/findAll',
        method: 'post',
        data
    })
}

//获取配料
export function getDishesIngredients() {
    const data = paramsSerializer([])
    return axios.request({
        url: 'http://localhost:8848/dishes/findIngredientsAll',
        method: 'post',
        data
    })
}

//获取步骤
export function getDishesStep() {
    const data = paramsSerializer([])
    return axios.request({
        url: 'http://localhost:8848/dishes/findStepAll',
        method: 'post',
        data
    })
}

//获取时间
export function getTime() {
    const data = paramsSerializer([])
    return axios.request({
        url: 'http://localhost:8848/dishes/time',
        method: 'post',
        data
    })
}

//提交菜类
export function postDishes(dishesId,dishesImg,dishesName,dishesDescribe,dishesCollect,dishesGoodnum,dishesTag,dishesUser,dishesTime) {
    const data = paramsSerializer([{
        name:'dishesId',
        value:dishesId
    },{
        name:'dishesImg',
        value:dishesImg
    },{
        name: 'dishesName',
        value: dishesName
    },{
        name: 'dishesDescribe',
        value: dishesDescribe
    },{
        name: 'dishesCollect',
        value: dishesCollect
    },{
        name: 'dishesGoodnum',
        value:dishesGoodnum
    },{
        name: 'dishesTag',
        value:dishesTag
    },{
        name: 'dishesUser',
        value:dishesUser
    },{
        name: 'dishesTime',
        value: dishesTime
    }

    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/setDishes',
        method: 'post',
        data
    })
}

//提交配料
export function postIngredients(ingredientsId,ingredientsDishesid,ingredientsName,ingredientsNum,ingredientsUnit) {
    const data = paramsSerializer([
        {
            name: 'ingredientsId',
            value: ingredientsId
        },
        {
            name: 'ingredientsDishesid',
            value: ingredientsDishesid
        },{
            name: 'ingredientsName',
            value: ingredientsName
        },{
            name: 'ingredientsNum',
            value: ingredientsNum
        },{
            name: 'ingredientsUnit',
            value: ingredientsUnit
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/setDishesIngredients',
        method: 'post',
        data
    })
}

//提交步骤
export function postStep(stepId,stepDishesid,stepImg,stepContent,stepNum) {
    const data = paramsSerializer([
        {
            name: 'stepId',
            value: stepId
        },
        {
            name: 'stepDishesid',
            value: stepDishesid
        },{
            name: 'stepImg',
            value: stepImg
        },{
            name: 'stepContent',
            value: stepContent
        },{
            name: 'stepNum',
            value: stepNum
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/setDishesStep',
        method: 'post',
        data
    })
}

//删除该菜及其步骤和配料
export function deleteDishes(dishesId) {
    const data = paramsSerializer([
        {
            name: 'dishesId',
            value: dishesId
        },
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/deleteDishes',
        method: 'post',
        data
    })
}

//删除单个步骤
export function mysqlDeleteStep(dishesStepId) {
    const data = paramsSerializer([
        {
            name: 'dishesStepId',
            value: dishesStepId
        },
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/deleteStep',
        method: 'post',
        data
    })
}

//删除单个配料
export function mysqlDeleteIngredients(dishesIngredientsId) {
    const data = paramsSerializer([
        {
            name: 'dishesIngredientsId',
            value: dishesIngredientsId
        },
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/deleteIngredients',
        method: 'post',
        data
    })
}

// 模糊查询
export function getLikeDishes(dishesName) {
    const data = paramsSerializer([
        {
            name: 'dishesName',
            value: dishesName
        },
    ])
    return axios.request({
        url: 'http://localhost:8848/dishes/likeDishes',
        method: 'post',
        data
    })
}

// 获取今日推荐
export function getTodayDishes() {
    const data = paramsSerializer([])
    return axios.request({
        url: 'http://localhost:8848/dishes/getTodayDishes',
        method: 'post',
        data
    })
}
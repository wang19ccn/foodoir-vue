import axios from 'axios'

//将要发送的数据构造成Form Data对象
function paramsSerializer(params) {
    let param = new URLSearchParams()
    for (let i = 0; i < params.length; i++) {
        param.append(params[i].name, params[i].value)
    }
    return param
}

//登录
export function loginAccount(username,password) {
    const data = paramsSerializer([
        {
            name: 'username',
            value: username
        },{
            name: 'password',
            value: password
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/login',
        method: 'post',
        data
    })
}

//注册时验证用户名是否已经存在
export function uniqueUsername(username) {
    const data = paramsSerializer([
        {
            name: 'username',
            value: username
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/checkUsername',
        method: 'post',
        data
    })
}

//注册账号密码
export function logonAccount(username,password) {
    const data = paramsSerializer([
        {
            name: 'username',
            value: username
        },{
            name: 'password',
            value: password
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/logonAccount',
        method: 'post',
        data
    })
}

//注册用户信息
export function logonInfo(accountId,infoImg,infoNickname,infoBirthday,infoSex) {
    const data = paramsSerializer([
        {
            name: 'infoImg',
            value: infoImg
        },{
            name: 'accountId',
            value: accountId
        },{
            name: 'infoNickname',
            value: infoNickname
        },{
            name: 'infoBirthday',
            value: infoBirthday
        },{
            name: 'infoSex',
            value: infoSex
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/logonInfo',
        method: 'post',
        data
    })
}

//修改密码
export function alterPass(userId,username,password) {
    const data = paramsSerializer([
        {
            name: 'userId',
            value: userId
        },
        {
            name: 'username',
            value: username
        },{
            name: 'password',
            value: password
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/logonAccount',
        method: 'post',
        data
    })
}

//获取用户信息
export function getInfo(userId) {
    const data = paramsSerializer([
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/getInfo',
        method: 'post',
        data
    })
}

//更改用户信息
export function alterInfo(infoId,accountId,infoImg,infoNickname,infoBirthday,infoSex,infoIntro) {
    const data = paramsSerializer([
        {
            name: 'infoId',
            value: infoId
        },
        {
            name: 'accountId',
            value: accountId
        },
        {
            name: 'infoImg',
            value: infoImg
        },{
            name: 'infoNickname',
            value: infoNickname
        },{
            name: 'infoBirthday',
            value: infoBirthday
        },{
            name: 'infoSex',
            value: infoSex
        }
        ,{
            name: 'infoIntro',
            value: infoIntro
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/logonInfo',
        method: 'post',
        data
    })
}

// 验证是否关注
export function checkAttention(ida,idb) {
    const data = paramsSerializer([
        {
            name: 'ida',
            value: ida
        },
        {
            name: 'idb',
            value: idb
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/checkAttention',
        method: 'post',
        data
    })
}

// 关注
export function setAttention(accountIda,accountIdb) {
    const data = paramsSerializer([
        {
            name: 'accountIda',
            value: accountIda
        },
        {
            name: 'accountIdb',
            value: accountIdb
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/setAttention',
        method: 'post',
        data
    })
}

// 取消关注
export function deleteAttention(ida,idb) {
    const data = paramsSerializer([
        {
            name: 'ida',
            value: ida
        },
        {
            name: 'idb',
            value: idb
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/deleteAttention',
        method: 'post',
        data
    })
}

// 验证是否收藏
export function checkCollect(dishesId,userId) {
    const data = paramsSerializer([
        {
            name: 'dishesId',
            value: dishesId
        },
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/checkCollect',
        method: 'post',
        data
    })
}

//收藏
export function setCollect(collectDishes,collectUser) {
    const data = paramsSerializer([
        {
            name: 'collectDishes',
            value: collectDishes
        },
        {
            name: 'collectUser',
            value: collectUser
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/setCollect',
        method: 'post',
        data
    })
}

//取消收藏
export function deleteCollect(dishesId,userId) {
    const data = paramsSerializer([
        {
            name: 'dishesId',
            value: dishesId
        },
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/deleteCollect',
        method: 'post',
        data
    })
}

// 验证是否点赞
export function checkGood(dishesId,userId) {
    const data = paramsSerializer([
        {
            name: 'dishesId',
            value: dishesId
        },
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/checkGood',
        method: 'post',
        data
    })
}

// 点赞
export function setGood(goodDishes,goodUser) {
    const data = paramsSerializer([
        {
            name: 'goodDishes',
            value: goodDishes
        },
        {
            name: 'goodUser',
            value: goodUser
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/setGood',
        method: 'post',
        data
    })
}

// 取消点赞
export function deleteGood(dishesId,userId) {
    const data = paramsSerializer([
        {
            name: 'dishesId',
            value: dishesId
        },
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/deleteGood',
        method: 'post',
        data
    })
}

//获取收藏列表
export function getListCollect(userId) {
    const data = paramsSerializer([
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/getCollect',
        method: 'post',
        data
    })
}

//获取关注列表
export function getListAttention(userId) {
    const data = paramsSerializer([
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/getAttention',
        method: 'post',
        data
    })
}

//获取粉丝列表
export function getListFans(userId) {
    const data = paramsSerializer([
        {
            name: 'userId',
            value: userId
        }
    ])
    return axios.request({
        url: 'http://localhost:8848/getFans',
        method: 'post',
        data
    })
}
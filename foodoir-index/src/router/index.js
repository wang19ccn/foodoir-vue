import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect:'index',
    children:[
      //-----首页
      {
        path:'/index',
        name:'Index',
        meta:{
          requireAuth:false
        },
        component: () => import('../components/index/Index.vue'),
      },
      //-----美食分享页
      {
        path: '/share',
        name: 'Share',
        meta:{
          requireAuth:false
        },
        component: () => import('../components/share/Index.vue')   
      },
      //-----美食记载页
      {
        path: '/record',
        name: 'Record',
        meta:{
          requireAuth:false
        },
        component: () => import('../components/record/Index.vue')   
      },
      //-----美食记载页
      {
        path: '/about',
        name: 'About',
        meta:{
          requireAuth:false
        },
        component: () => import('../components/about/Index.vue')   
      },
      //-----个人中心
      {
        path: '/person',
        name: 'Person',
        redirect:'/person/myPublish',
        meta:{
          requireAuth:true
        },
        component: () => import('../components/person/Index.vue'), 
        children:[
          //-----个人中心-------我的发布
          {
            path:'/person/myPublish',
            name:'MyPublish',
            meta:{
              requireAuth:true
            },
            component: () => import('../components/person/util/MyPublish'),
          },
           //-----个人中心-------我的收藏
           {
            path:'/person/myCollect',
            name:'MyCollect',
            meta:{
              requireAuth:false
            },
            component: () => import('../components/person/util/MyCollect'),
          },
          //-----个人中心-------我的关注
          {
            path:'/person/myAttention',
            name:'MyAttention',
            meta:{
              requireAuth:false
            },
            component: () => import('../components/person/util/MyAttention'),
          },
          //-----个人中心-------我的粉丝
          {
            path:'/person/myFans',
            name:'MyFans',
            meta:{
              requireAuth:false
            },
            component: () => import('../components/person/util/MyFans'),
          },
          //-----个人中心-------个人设置
          {
            path:'/person/mySetting',
            name:'MySetting',
            meta:{
              requireAuth:false
            },
            component: () => import('../components/person/util/MySetting'),
          }
        ] 
      },
      //-----登录页面
      {
        path: '/login',
        name: 'Login',
        meta:{
          requireAuth:false
        },
        component: () => import('../components/register/Login.vue')
      }
    ]
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

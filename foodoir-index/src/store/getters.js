export default {
    //侧栏分别选择
    inforMenus(state) {
        //全部
        if (state.sideKey == 0) {
            return state.menus
        }
        //原创
        if (state.sideKey == 1) {
            return state.menus.filter(x => x.dishesTag === '原创')
        }
        //范本
        if (state.sideKey == 2) {
            return state.menus.filter(x => x.dishesTag === '范本')
        }
        //翻制
        if (state.sideKey == 3) {
            return state.menus.filter(x => x.dishesTag === '翻制')
        }
    },
    //每日一菜
    todayDishes(state){
        return state.todayDishes
    },
    //热门推荐
    hotDishes(state){
        return state.menus.slice(0,3)
    },
    //优秀美食
    goodDishes(state){
        return state.menus.filter(x => x.dishesGoodnum >= 500)
    },
    //获取菜的详细信息
    getDishesInfo(state){
        if(0==state.dishesId)
        {
            let nullDishesInfo =[{dishesId: '',
            dishesImg: '',
            dishesName: '',
            dishesDescribe: '',
            dishesIngredients: [{
                ingredientsId: '',
                ingredientsName: '',
                ingredientsNum: '',
                ingredientsUnit: '',
            }],
            dishesStep: [{
                stepId: '',
                stepImg: '',
                stepContent: '',
                stepNum: 0,
            }],
            dishesCollect: 0,
            dishesGoodnum: 0,
            dishesTag: '',
            dishesUser: '',
            dishesTime: '',}]
            return nullDishesInfo 
        }
        let dishesInfo=state.menus.filter(x=> x.dishesId===state.dishesId)
        return dishesInfo
    },
    //获取个人发布
    getMyPublish(state) {
        let myPublish=state.menus.filter(x=> x.dishesUser===state.user.userId)
        return myPublish
    },

}
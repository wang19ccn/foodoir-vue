// import axios from 'axios'

import {getDishes,getDishesIngredients,getDishesStep,getLikeDishes} from '@/api/dishesApi.js';

import {getInfo} from '@/api/accountApi.js';

export default {
    //前端模拟数据
    // getDishes(context) {
    //     axios.get('http://localhost:8080/dishes_data.json').then(({
    //         data
    //     }) => {
    //         console.log(data)
    //         context.commit('initMenus', data)
    //     })
    // },
    //获取菜类信息
    foodoirGetDishes(context){
        getDishes().then(resp=>{
            context.commit('initMenus', resp.data)
        })
    },
    //获取配料信息
    foodoirGetDishesIngredients(context){
        getDishesIngredients().then(resp=>{
            context.commit('initIngredients', resp.data)
        })
    },
    //获取步骤信息
    foodoirGetDishesStep(context){
        getDishesStep().then(resp=>{
            context.commit('initSteps', resp.data)
        })
    },
    
    //获取用户信息
    getUserInfo(context,id){
        getInfo(id).then(resp=>{
            //console.log(this.$store.state.user.userId)
            console.log(resp)
            context.commit('setUserInfo', resp.data)
        })
    },

    //模糊查询
    getLike(context,dishesName){
        getLikeDishes(dishesName).then(resp=>{
            context.commit('initMenus', resp.data);
        })
    }
}
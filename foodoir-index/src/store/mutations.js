export default {
    //-----------用户信息相关数据操作----------------
    login(state, user) {
        state.user = user
        window.localStorage.setItem('user', JSON.stringify(user))
    },
    setUserInfo(state, userInfo) {
        state.userInfo = userInfo
    },
    setUserCollect(state,userCollect){
        state.userCollect=userCollect
    },
    setUserFans(state,userFans){
        state.userFans=userFans
    },
    setUserAttention(state,userAttention){
        state.userAttention=userAttention
    },
    

    //-----------菜类信息相关数据操作----------------
    //初始化 menus 数据
    initMenus(state, dishes) {
        state.menus = dishes
    },
    //初始化 steps 数据
    initSteps(state, steps) {
        state.steps = steps
    },
    //初始化 ingredients 数据
    initIngredients(state, ingredients) {
        state.ingredients = ingredients
    },
    //获取对应菜类的发布者信息
    getDishesUserInfo(state,dishesUserInfo){
        state.dishesUserInfo=dishesUserInfo;
    },
    //检查是否收藏
    setIsCollect(state,isCollect){
        state.vuexIsCollect=isCollect;
    },
    //检查是否点赞
    setIsGood(state,isGood){
        state.vuexIsGood=isGood;
    },
    //检查是否关注
    setIsAttention(state,isAttention){
        state.vuexIsAttention=isAttention;
    },
    //获取今日推荐
    setTodayDishes(state,today){
        state.todayDishes=today;
    },


    //-----------组件控制相关数据操作----------------
    //侧边栏选中值
    selectKey(state, key) {
        state.sideKey = key;
    },
    //导航栏选中，防字刷新后消失选中效果
    selectNav(state, index) {
        state.navChoose = index;
    },
    //当点击查看菜的详细信息时，获取菜的id号
    selectDishesId(state, id) {
        state.dishesId = id;
    }
}
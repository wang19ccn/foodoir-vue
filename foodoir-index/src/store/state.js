export default {
    //-----------用户信息----------------
    user: {
        username: window.localStorage.getItem('user' || '[]') == null ? '' : JSON.parse(window.localStorage.getItem('user' || '[]')).username,
        userId:  window.localStorage.getItem('user' || '[]') == null ? '' : JSON.parse(window.localStorage.getItem('user' || '[]')).userId
    },
    userInfo:{},
    userCollect:[],
    userFans:[],
    userAttention:[],

    //-----------菜类信息----------------
    menus: [],
    steps: [],
    ingredients:[],
    dishesAll:[],
    dishesUserInfo:{},
    todayDishes:{},

    vuexIsCollect:true,//检查是否收藏
    vuexIsGood:true,//检查是否点赞
    vuexIsAttention:true,//检查是否关注

    //-----------组件控制----------------
    sideKey: 0,
    navChoose: '/index',
    dishesId:0,
}